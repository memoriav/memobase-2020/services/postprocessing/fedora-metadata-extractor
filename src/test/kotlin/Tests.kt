/*
 * record-parser
 * Copyright (C) 2019  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import java.io.File
import java.io.InputStream
import java.nio.charset.Charset
import java.util.stream.Stream
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Tests {
    private val log = LogManager.getLogger("TestLogger")

    private val resourcePath = "src/test/resources/data"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    private fun createInputStream(fileName: String): InputStream {
        return File("$resourcePath/$fileName").inputStream()
    }

    private val regex = Regex("(_:B[A-Za-z0-9]+)")

    @Test
    fun `test json ld writer`() {
        val model = ModelFactory.createDefaultModel()
        RDFDataMgr.read(model, createInputStream("jsonld-test-input.nt"), Lang.NTRIPLES)

        val result = Functions.writeModel(model)
        assertThat(result)
            .isEqualTo(readFile("jsonld-test-output.json"))
    }

    @ParameterizedTest
    @MethodSource("kafkaTests")
    fun `test kafka topology`(params: KafkaTestParams) {
        /*
        val service = Service(params.settingsFileName)
        service.run()
        assert(true)
         */

        /*
        val testDriver = TopologyTestDriver(service.topology, service.settings.kafkaStreamsSettings)
        val factory = ConsumerRecordFactory(StringSerializer(), StringSerializer())
        val input = readFile("${params.count}/input.json")
        val output = readFile("${params.count}/output.nt")
        testDriver.pipeInput(
            factory.create(service.settings.inputTopic, params.key, input)
        )

        val record = testDriver.readOutput(
            service.settings.outputTopic,
            StringDeserializer(),
            StringDeserializer()
        )
        assertThat(record).isNotNull

        val sortedResult = record.value().lines().map {
            var replacedString = it
            for (matchResult in regex.findAll(it)) {
                replacedString = replacedString.replace(matchResult.groups[0]?.value.orEmpty(), "_:B")
            }
            replacedString
        }.sorted().reduce { acc, s -> acc + "\n" + s }
        assertThat(sortedResult)
            .isEqualTo(output)
        */
    }

    private fun kafkaTests() = Stream.of(
        KafkaTestParams(
            1,
            "test1.yml",
            null
        )
    )
}

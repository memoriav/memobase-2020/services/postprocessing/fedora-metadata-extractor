package org.memobase.model

data class EventMessage(
    val eventId: String,
    val eventTimestamp: String,
    val eventType: String,
    var objectPath: String,
    val objectType: String,
    val objectVersion: String? = null
)

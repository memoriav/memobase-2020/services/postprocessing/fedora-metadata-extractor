package org.memobase

import java.io.StringWriter
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat

object Functions {
    fun writeModel(model: Model): String {
        val writer = StringWriter()
        RDFDataMgr.write(writer, model, RDFFormat.JSONLD_COMPACT_FLAT)
        return writer.toString().trim()
    }
}

FROM gradle:6.3-jdk8
ADD . /
WORKDIR /
RUN gradle --no-daemon --no-scan --no-build-cache distTar
RUN cd /build/distributions && tar xf app.tar

FROM openjdk:8-jre-alpine
COPY --from=0 /build/distributions/app /app
CMD /app/bin/fedora-metadata-extractor
